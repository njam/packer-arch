#!/bin/bash

set -eux

PASSWORD=$(openssl passwd -crypt 'vagrant')
useradd --password "${PASSWORD}" --create-home vagrant
echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant
chmod 0440 /etc/sudoers.d/10_vagrant
systemctl start sshd.service
