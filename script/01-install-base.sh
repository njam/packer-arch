#!/bin/bash

set -eux

DISK='/dev/vda'
KEYMAP='us'
LOCALE='en_US.UTF-8'
TIMEZONE='Europe/Zurich'
COUNTRY='US'

ROOT_PARTITION="${DISK}1"
TARGET_DIR='/mnt'
MIRRORLIST="https://www.archlinux.org/mirrorlist/?country=${COUNTRY}&protocol=http&protocol=https&use_mirror_status=on"


# Pacman mirrors
curl -s "${MIRRORLIST}" |  sed 's/^#Server/Server/' > /etc/pacman.d/mirrorlist


# Filesystem
sgdisk --zap ${DISK}
dd if=/dev/zero of=${DISK} bs=512 count=2048
wipefs --all ${DISK}
sgdisk --new=1:0:0 ${DISK}
sgdisk ${DISK} --attributes=1:set:2
mkfs.ext4 -O ^64bit -F -m 0 -q -L root ${ROOT_PARTITION}
mount -o noatime,errors=remount-ro ${ROOT_PARTITION} ${TARGET_DIR}


# Bootstrap + fstab
pacstrap "${TARGET_DIR}" base linux linux-firmware openssh
genfstab -L "${TARGET_DIR}" >> "${TARGET_DIR}/etc/fstab"


# Enter chroot and run setup
install --mode=0755 /dev/null "${TARGET_DIR}/arch-setup.sh"
cat <<-EOF > "${TARGET_DIR}/arch-setup.sh"
	# Setup time
	ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
	hwclock --systohc

	# Keymap and locale
	echo "KEYMAP=${KEYMAP}" > /etc/vconsole.conf
	sed -i "s/#${LOCALE}/${LOCALE}/" /etc/locale.gen
	locale-gen

	# mkinitcpio
	/usr/bin/mkinitcpio -p linux
EOF
arch-chroot "${TARGET_DIR}" "/arch-setup.sh"
rm "${TARGET_DIR}/arch-setup.sh"


# Reboot
exit
systemctl reboot
