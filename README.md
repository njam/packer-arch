packer-arch
===========

Building an Arch Linux image/ISO using Packer.


Building the Image
------------------
Build qemu image:
```
packer build arch.json
```


Resources
---------

Packer configs for Arch:
- https://github.com/elasticdog/packer-arch/
- https://github.com/kaorimatz/packer-templates/blob/master/archlinux-x86_64.json
- https://github.com/lavabit/robox/blob/master/generic-libvirt.json#L370

Arch install script:
- https://disconnected.systems/blog/archlinux-installer/#the-complete-installer-script

